TARGET=simpol.x86_64
CXXFLAGS=-O2 -std=c++11 -Og -ggdb
LDFLAGS=-lstdc++

CC=g++

all: precompiled.h.gch
	@$(CC) main.cpp $(CXXFLAGS) -o $(TARGET) $(LDFLAGS)

precompiled.h.gch: precompiled.h
	@$(CC) precompiled.h

run: all
	@./$(TARGET) test.smpl
