SIMPOL compiler for PDMA platform.
Translates code to PDASM and then uses PDASM compiler to compile to final binary.

Types:
	int8,uint8,int16,uint16,int32,uint32,string,none
int8	- values from -128 to 127
uint8	- values from 0 to 255
int16	- values from -32768 to 32767
uint16	- values from 0 to 65535
int32	- values from -2147483648 to 2147483647
uint32	- values from 0 to 4294967296
int	- same as int32
uint	- same as uint32
string	- string type (for text) // just stores address of string start
none	- type for functions, means nothing, can't create vars with this type

Operators:
	arithmetic operators: +, -, *, /, %
	assignment operators: =, +=, -=, *=, /=, %=
	comparasion operators: ==, !=, >, <, >=, <=
	binary operators: inv (~), and (&), or (|), xor (^), shl (<<), shr (>>)
	boolean algebra operators: and (&&), or (||), not (!)
	memory managment operators: [addr] (take value from address "addr" )

Function calling: 
	f arg1, arg2, arg3
Example:
	int a = f 5
	int b = fac 3
	int c = (a+b)
	int d = (f 5) + (fac 3)
	// (c == d) == true

Function declaration:
	function f type1 arg1, type2 arg2, type3 arg3 returns type_r
		...
	end
Function without return value can be declared as function and as procedure, like this:
	func f type1 arg1, type2 arg2 returns none
		...
	end
	proc f type1 arg1, type2 arg2
		...
	end
Example:
	func fac int32 n returns int32
		int32 r = 1
		while n > 0
			r *= n
			n -= 1
		end
		return r
	end
	print "fac(5) = ", (fac 5), '\n'
Braces "()" are optional, just for programmer.
