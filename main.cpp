#include "precompiled.h"
#define S_LOG(s) cout << s << endl
#define INC_PATH "./"

/*
	=================================
*/
namespace simpol
{
	enum VAR_TYPE
	{
		ERROR_T=0,	// no type (invalid)
		INT8_T=1,
		UINT8_T=2,
		INT16_T=3,
		UINT16_T=4,
		INT32_T=5,
		UINT32_T=6,
		INT_T=INT32_T,
		UINT_T=UINT32_T,
		STRING_T=7,
		NONE_T=8,	// aka "void"
		FLOAT_T=9,
	};

	/*
		Variable descriptor
		Has variable name and type. Used for correct context work
	*/
	struct var_desc
	{
		var_desc(const string &_name="", VAR_TYPE _type=VAR_TYPE::ERROR_T, int pos=0)
			: name(_name), type(_type), stck_pos(pos) { }
		void setPos(int pos)
		{ stck_pos = pos; }

		string name;
		int stck_pos; // pos in stack (in dwords)
		VAR_TYPE type;
	};

	/*
		Descriptor for functions and procedures
		Procedure descriptor is the same as function descriptor,
		but has r_type == NONE_T.
	*/
	struct func_desc
	{
		string name;
		vector<var_desc> args;
		VAR_TYPE r_type;		// function return type
	};

	struct context_t
	{
		context_t()
		{
			c_parent = c_child = nullptr;
		}

		void attachTo(context_t *parent)
		{
			if(c_parent)
				c_parent->c_child = nullptr;
			c_parent = parent;
			if(c_parent)
				c_parent->c_child = this;
		}

		

		context_t &operator =(int a)
		{
			return *this;
		}
		vector<var_desc> c_vars;
		context_t *c_parent;
		context_t *c_child;
	};
	
	typedef int ctx_t;
}
/*
	=================================
*/

bool is_ignorable(char c)
{
	if(c == ' ' or c == '\t' or c == '\r' or c == '\n')
		return true;
	return false;
}

void trim_string(string &s)
{
	ssize_t start=0, stop=s.length();

	if(is_ignorable(s[0]))
	{
		for(int i=0; i < s.length()-1; i++)
		{
			if(is_ignorable(s[i]) and !is_ignorable(s[i+1]))
			{
				start = i+1;
				break;
			}
		}
	}

	if(is_ignorable(s[s.length()-1]))
	{
		for(int i=s.length()-1; i > 0; --i)
		{
			if(is_ignorable(s[i]) and !is_ignorable(s[i-1]))
			{
				stop = i-1;
				break;
			}
		}
	}
	s = s.substr(start, stop-start);
}

int strncmpi(const string &s1, const string &s2, int len)
{
	for(int i=0; i < len; i++)
	{
		if( (s1[i]|0x20) != (s2[i]|0x20)) // non utf-8
			return 1;
	}
	return 0;
}

bool streqni(const string &s1, const string &s2, int len)
{
	return (strncmpi(s1, s2, len) == 0);
}

void strip_comments(string &s)
{
	if(s.length() < 2)
		return;
	ssize_t stop=s.length();
	for(int i=0; i < s.length()-1; i++)
	{
		if(s[i] == '/' and s[i+1] == '/')
		{
			stop = i;
			break;
		}
	}
	s = s.substr(0, stop);
}

void read_lines(const string &fname, vector<string> &lines)
{
	ifstream fin;
	fin.open(fname);
	if(!fin.is_open())
	{
		cerr << "Failed to open file \"" << fname << "\"!" << endl;
		return;
	}
	string line;
	while(getline(fin, line, '\n'))
	{
		lines.push_back(line);
	}
}

/*
	Preprocessing function
	Resolves macros, stripps code
	@src - input (source program text)
	@out - output vector of code lines (appending to it, not clearing)
*/
void preprocess_code(const vector<string> &src, vector<string> &out)
{
#pragma message("ToDo:Macros (defines)")
	for(string line : src)
	{
		trim_string(line);
		strip_comments(line);
		if(line.empty())
			continue;
		if(line[0] == '#')
		{
			if(streqni(line, "#include", 8))
			{
				size_t ss, se;
				ss = line.find_first_of('"')+1;
				se = line.find_last_of('"');
				string inc_file = line.substr(ss, se-ss);
				
				vector<string> inc_lines;
				read_lines(INC_PATH+inc_file, inc_lines);
				preprocess_code(inc_lines, out);
				continue;
			}
		}
		else
			out.push_back(line);
	}	
}

string extract_string(const string &s)
{
	ssize_t start, stop;
	start = s.find_first_of('"')+1;
	if(start == string::npos)
		return "";
	stop  = s.find_last_of('"');
	if(stop == string::npos)
		return "";
	string res = s.substr(start, stop-start);
	return res;
}

simpol::VAR_TYPE get_vartype(const string &line)
{
	using namespace simpol;
	if(streqni(line, "int8", 4))
		return VAR_TYPE::INT8_T;
	if(streqni(line, "uint8", 5))
		return VAR_TYPE::UINT8_T;
	if(streqni(line, "int16", 5))
		return VAR_TYPE::INT16_T;
	if(streqni(line, "uint16", 6))
		return VAR_TYPE::UINT16_T;
	if(streqni(line, "int32", 5) or streqni(line, "int", 3))
		return VAR_TYPE::INT32_T;
	if(streqni(line, "uint32", 6) or streqni(line, "uint", 4))
		return VAR_TYPE::UINT32_T;
	if(streqni(line, "float", 5))
		return VAR_TYPE::FLOAT_T;
	if(streqni(line, "string", 6))
		return VAR_TYPE::STRING_T;
	if(streqni(line, "none", 4))
		return VAR_TYPE::NONE_T;
	return VAR_TYPE::ERROR_T;
}

int get_varval(const string &line)
{
	int res = 0;
	if(line.find_first_of('=') != string::npos)
	{
		string vstr = line.substr(line.find_first_of('=')+1);
		res = stoi(vstr);
	}
	return res;
}
	
string compiled_line(simpol::ctx_t &ctx, const string &line)
{
	string res;
	if(ctx == 0)
	{
		// avg code
		simpol::VAR_TYPE decl_var = get_vartype(line);
		if(decl_var) // not 0
		{
			assert(decl_var != simpol::VAR_TYPE::NONE_T and
				   decl_var != simpol::VAR_TYPE::STRING_T);
			int val = get_varval(line);
			
			char *buff = new char[32];
			memset(buff, 0, 16);
			sprintf(buff, "push $0x%04x", val);
			res = buff;
			delete[] buff;
		}
		else
		{
			
		}
	}
	else if(ctx == 3) // assembly
	{
		res = line;
	}
	
	return res;
}

void compile_code(const vector<string> &code, vector<string> &out_asm)
{
	out_asm.clear();
	int in_ctx = 0, no_ctx = 0; // no_ctx is context counter (unique)
#pragma message("ToDo:Contexts")
	simpol::ctx_t context = 0; // for now
	for(const string &line : code)
	{
		if(streqni(line, "end", 3))
		{
			assert(in_ctx>0);
			--in_ctx;
			context = 0; // switch context
			continue;
		}
		
		bool is_if		= streqni(line, "if", 2);
		bool is_while	= streqni(line, "while", 5);
		bool is_for		= streqni(line, "for", 3);
		bool is_func	= streqni(line, "function", 8) or
						  streqni(line, "func", 4);
		bool is_proc	= streqni(line, "procedure", 9) or
						  streqni(line, "proc", 4);
		bool is_asm		= streqni(line, "assembly", 8) or
						  streqni(line, "asm", 3);
		
		if(is_if or is_while or is_for or is_func or is_proc or is_asm)
			in_ctx++;
		
		if(is_asm)
		{
			context = 3;
			continue;
		}
		
		out_asm.push_back(compiled_line(context, line));
	}
}

int main(int argc, char **argv)
{
	if(argc < 2)
		return 1;

	string srcFile = argv[1];
	vector<string> code_lines, formatted_lines, out_asm_lines;
	read_lines(srcFile, code_lines);
	preprocess_code(code_lines, formatted_lines);
	compile_code(formatted_lines, out_asm_lines);
	
	for(const string &line : out_asm_lines)
		cout << line << endl;
	return 0;
}
